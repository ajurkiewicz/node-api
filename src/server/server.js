const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const emailRouter = require('../emails/router')
const cors = require('cors')

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
emailRouter(app)

module.exports = app