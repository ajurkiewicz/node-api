require('../emails/mailer.mocks').mockMailer()
const app = require('./server')
const db = require('../db/mongodb')
const supertest = require('supertest')
const request = supertest(app)
const Email = require('../emails/model')
const { statuses: { pending: PENDING, sent: SENT }, priorities: { normal: NORMAL }, result: { statusCodes } } = require('../emails/config')
const { success: SUCCESS, created: CREATED } = statusCodes
const { predefinedEmails: { correctEmail } } = require('../dev/testInputData')
const { emails: initEmailsCollection } = require('../dev/testDB.json')
const { baseUri: BASE_URI } = require('../emails/config')

describe('Api tests', () => {
    const { GATEWAY } = process.env

    beforeAll(async () => {
        await db.connect()
    })

    beforeEach(async () => {
        await Email.deleteMany({})
        await Email.insertMany(initEmailsCollection)
    })

    afterAll(async () => {
        await db.disconnect()
    })

    it('should return correct response /api/emails request without pagination params(first,last,next)', async () => {
        const result = await request.get('/api/emails')
        expect(result.status).toBe(SUCCESS)
        expect(result.body).toEqual({
            data: {
                emails: initEmailsCollection.slice(0,25),
                emailsPerPage: 25,
                totalEmailsCount: 100
            },
            links: {
                "first": `${GATEWAY}${BASE_URI}?page=1&limit=25`,
                "next": `${GATEWAY}${BASE_URI}?page=2&limit=25`,
                "last": `${GATEWAY}${BASE_URI}?page=4&limit=25`
            }
        })
        expect(result.body.data.emails.length).toEqual(25)
    })

    it('should return correct response /api/emails request with only limit pagination parameter', async () => {
        const result = await request.get('/api/emails?limit=88')
        expect(result.status).toBe(SUCCESS)
        expect(result.body).toEqual({
            data: {
                emails: initEmailsCollection.slice(0,88),
                emailsPerPage: 88,
                totalEmailsCount: 100
            },
            links: {
                "first": `${GATEWAY}${BASE_URI}?page=1&limit=88`,
                "next": `${GATEWAY}${BASE_URI}?page=2&limit=88`,
                "last": `${GATEWAY}${BASE_URI}?page=2&limit=88`
            }
        })
        expect(result.body.data.emails.length).toEqual(88)
    })

    it('should return correct response /api/emails with pagination params (first,last)', async () => {
        const result = await request.get('/api/emails?page=1&limit=100')
        expect(result.status).toBe(SUCCESS)
        expect(result.body).toEqual({
            data: {
                emails: initEmailsCollection,
                emailsPerPage: 100,
                totalEmailsCount: 100
            },
            links: {
                "first": `${GATEWAY}${BASE_URI}?page=1&limit=100`,
                "last": `${GATEWAY}${BASE_URI}?page=1&limit=100`
            }
        })
        expect(result.body.data.emails.length).toEqual(initEmailsCollection.length)
    })

    it('should return correct response /api/emails with pagination params (first,last,prev,next)', async () => {
        const result = await request.get('/api/emails?page=3&limit=10')
        expect(result.status).toBe(SUCCESS)
        expect(result.body).toEqual({
            data: {
                emails: initEmailsCollection.slice(20, 30),
                emailsPerPage: 10,
                totalEmailsCount: 100
            },
            links: {
                "first": `${GATEWAY}${BASE_URI}?page=1&limit=10`,
                "last": `${GATEWAY}${BASE_URI}?page=10&limit=10`,
                "prev": `${GATEWAY}${BASE_URI}?page=2&limit=10`,
                "next": `${GATEWAY}${BASE_URI}?page=4&limit=10`
            }
        })
        expect(result.body.data.emails.length).toEqual(10)
    })

    it('should return correct response /api/emails with pagination params (first,last,prev)', async () => {
        const result = await request.get('/api/emails?page=10&limit=10')
        expect(result.status).toBe(SUCCESS)
        expect(result.body).toEqual({
            data: {
                emails: initEmailsCollection.slice(90, 100),
                emailsPerPage: 10,
                totalEmailsCount: 100
            },
            links: {
                "first": `${GATEWAY}${BASE_URI}?page=1&limit=10`,
                "last": `${GATEWAY}${BASE_URI}?page=10&limit=10`,
                "prev": `${GATEWAY}${BASE_URI}?page=9&limit=10`
            }
        })
        expect(result.body.data.emails.length).toEqual(10)
    })

    it('should return empty (correct) response /api/emails with wrong pagination params', async () => {
        const result = await request.get('/api/emails?page=20&limit=12')
        expect(result.status).toBe(SUCCESS)
        expect(result.body).toEqual({
            data: {
                emails: [],
                emailsPerPage: 12,
                totalEmailsCount: 100
            },
            links: {
                "first": `${GATEWAY}${BASE_URI}?page=1&limit=12`,
                "last": `${GATEWAY}${BASE_URI}?page=9&limit=12`
            }
        })
        expect(result.body.data.emails.length).toEqual(0)
    })

    it('should return email status /api/emails/:id/status', async () => {
        const _id = initEmailsCollection[0]._id
        const result = await request.get(`/api/emails/${_id}/status`)
        expect(result.status).toBe(SUCCESS)
        expect(result.body).toEqual({
            data: {
                status: initEmailsCollection[0].status
            }
        })
    })

    it('should create new email /api/emails post request', async () => {
        const result = await request
            .post('/api/emails')
            .send(correctEmail)

        const { sender, recipients, body, title } = correctEmail

        expect(result.status).toBe(CREATED)
        expect(result.body.data.sender).toEqual(sender)
        expect(result.body.data.recipients).toEqual(recipients)
        expect(result.body.data.body).toEqual(body)
        expect(result.body.data.title).toEqual(title)
        expect(result.body.data.status).toEqual(PENDING)
        expect(result.body.data.priority).toEqual(NORMAL)
    })

    it('should return email details /api/emails/:id', async () => {
        const _id = initEmailsCollection[0]._id
        const result = await request.get(`/api/emails/${_id}`)
        const {
            status,
            sender,
            recipients,
            body,
            priority
        } = initEmailsCollection[0]
        expect(result.status).toBe(SUCCESS)
        expect(result.body.data.status).toBe(status)
        expect(result.body.data.recipients).toEqual(recipients)
        expect(result.body.data.body).toBe(body)
        expect(result.body.data.priority).toBe(priority)
        expect(result.body.data.sender).toBe(sender)

    })

    it('should return emails statuses (pending,sent) /api/emails/status ', async () => {
        const result = await request.get('/api/emails/status')
        expect(result.status).toBe(SUCCESS)
        expect(result.body).toEqual({
            data: {
                pending: 3,
                sent: 97
            }
        })
    })

    it('should send email /api/emails/:id/send', async () => {
        const _id = initEmailsCollection[0]._id
        const result = await request.patch(`/api/emails/${_id}/send`)
        expect(result.status).toBe(SUCCESS)
        expect(result.body).toEqual({
            data: {
                ...initEmailsCollection[0],
                status: SENT
            }
        })
    })

    it('should send emails /api/emails/send', async () => {
        const result = await request.patch('/api/emails/send')
        expect(result.status).toBe(SUCCESS)
        expect(result.body).toEqual({
            data: {
                n: 3,
                nModified: 3,
                ok: 1
            }
        })
    })
})