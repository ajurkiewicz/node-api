const mongoose = require('mongoose')
const { connectionUrl } = require('./config')

const connect = () => {
    mongoose.connect(connectionUrl, { useNewUrlParser: true, useUnifiedTopology: true })
}

const disconnect = () => {
    mongoose.connection.close()
}

module.exports = {
    connect,
    disconnect
}