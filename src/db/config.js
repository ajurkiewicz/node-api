const baseUrl = 'mongodb://127.0.0.1'
const port = 27017
const databaseName = 'emails-api'
const connectionUrl = `${baseUrl}:${port}/${databaseName}`

module.exports = {
    baseUrl,
    port,
    databaseName,
    connectionUrl
}