const port = process.env.PORT || 8080
const app = require('./server/server')
const mongoose = require('mongoose')
const db = require('./db/mongodb')

mongoose.connection.once('open', () => {
    console.log(`Connected to MongoDB...`)
    console.log(`Application listening on port ${port}...`)
    process.env.GATEWAY = `http://localhost:${port}`
    app.listen(port)
})

mongoose.connection.on('error', () => {
    console.log(`Could not connect to MongoDB and start application...`)
})

db.connect()


