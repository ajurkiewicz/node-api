const { statuses: { sent }, priorities: { low } } = require('../emails/config')

const correctEmail = {
    sender: 'test@test.pl',
    recipients: [
        'test@test.pl',
        'test2@test.pl'
    ],
    body: "test body",
    title: "title body"
}

const emailWithIncorrectSender = {
    sender: 'test.test.pl',
    recipients: [
        'test@test.pl',
        'test2@test.pl'
    ],
    body: "test body",
    title: "title body"
}

const emailWithIncorrectRecipients = {
    sender: 'test@test.pl',
    recipients: [
        'test.test.pl',
        'test.test.pl'
    ],
    body: "test body",
    title: "title body"
}

const emailWithOneIncorrectRecipient = {
    sender: 'test@test.pl',
    recipients: [
        'test@test.pl',
        'test.test.pl'
    ],
    body: "test body",
    title: "title body"
}

const emailWithDefaultValuesOverridden = {
    sender: 'test@test.pl',
    recipients: [
        'test@test.pl',
        'test@test.pl'
    ],
    body: "test body",
    title: "title body",
    priority: low,
    status: sent
}



module.exports = {
    predefinedEmails: {
        correctEmail,
        emailWithIncorrectSender,
        emailWithIncorrectRecipients,
        emailWithOneIncorrectRecipient,
        emailWithDefaultValuesOverridden
    }
}