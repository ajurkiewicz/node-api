const { getPaginationLinks, getDBQueryOptionsForPagination, getParsedPaginationParams } = require('./utils')
const { baseUri: BASE_URI, pagination: { limit: defaultLimit } } = require('../emails/config')

describe('Utils tests', () => {
    const { GATEWAY }= process.env

    it('should prepare pagination without prev', () => {
        const links = getPaginationLinks({
            page: 1,
            limit: 10,
            emailsCount: 100
        })

        expect(links).toEqual({
            first: `${GATEWAY}${BASE_URI}?page=1&limit=10`,
            last: `${GATEWAY}${BASE_URI}?page=10&limit=10`,
            next: `${GATEWAY}${BASE_URI}?page=2&limit=10`
        })
    })

    it('should prepare pagination without next', () => {
        const links = getPaginationLinks({
            page: 2,
            limit: 60,
            emailsCount: 100
        })
        expect(links).toEqual({
            first: `${GATEWAY}${BASE_URI}?page=1&limit=60`,
            last: `${GATEWAY}${BASE_URI}?page=2&limit=60`,
            prev: `${GATEWAY}${BASE_URI}?page=1&limit=60`
        })
    })

    it('should prepare pagination without prev and next', () => {
        const links = getPaginationLinks({
            page: 1,
            limit: 120,
            emailsCount: 100
        })
        expect(links).toEqual({
            first: `${GATEWAY}${BASE_URI}?page=1&limit=120`,
            last: `${GATEWAY}${BASE_URI}?page=1&limit=120`
        })
    })

    it('should prepare pagination without prev and next (page id undefined)', () => {
        const links = getPaginationLinks({
            page: undefined,
            limit: 100,
            emailsCount: 100
        })
        expect(links).toEqual({
            first: `${GATEWAY}${BASE_URI}?page=1&limit=100`,
            last: `${GATEWAY}${BASE_URI}?page=1&limit=100`
        })
    })

    it('should prepare pagination without prev and next if pageId is incorrect', () => {
        const links = getPaginationLinks({
            page: -1,
            limit: 20,
            emailsCount: 100
        })
        expect(links).toEqual({
            first: `${GATEWAY}${BASE_URI}?page=1&limit=20`,
            last: `${GATEWAY}${BASE_URI}?page=5&limit=20`
        })
    })

    it('should prepare pagination without prev if page id is undefined', () => {
        const links = getPaginationLinks({
            page: undefined,
            limit: 25,
            emailsCount: 100
        })
        expect(links).toEqual({
            first: `${GATEWAY}${BASE_URI}?page=1&limit=25`,
            last: `${GATEWAY}${BASE_URI}?page=4&limit=25`,
            next: `${GATEWAY}${BASE_URI}?page=2&limit=25`
        })
    })

    it('should prepare query with skip value = 0', () => {
        const links = getDBQueryOptionsForPagination({
            page: 1,
            limit: 120
        })
        expect(links).toEqual({
            skip: 0
        })
    })

    it('should prepare query options with skip value > 0', () => {
        const links = getDBQueryOptionsForPagination({
            page: 5,
            limit: 10
        })
        expect(links).toEqual({
            skip: 40
        })
    })

    it('should prepare query options without skip property if pageId is undefined', () => {
        const links = getDBQueryOptionsForPagination({
            page: undefined,
            limit: 10
        })
        expect(links).toEqual({})
    })

    it('should prepare query options without skip property if pageId < 0', () => {
        const links = getDBQueryOptionsForPagination({
            page: -1,
            limit: 10
        })
        expect(links).toEqual({})
    })

    it('should correctly parse query params if undefined is passed', () => {
        const params = getParsedPaginationParams({
            page: undefined,
            limit: undefined
        })
        expect(params).toEqual({ page: 0, limit: defaultLimit })
    })

    it('should correctly parse query params if page < 0 and limit < 0', () => {
        const params = getParsedPaginationParams({
            page: -2,
            limit: -30
        })
        expect(params).toEqual({ page: 0, limit: defaultLimit })
    })

    it('should correctly parse query params if page and limit are correctly defined', () => {
        const params = getParsedPaginationParams({
            page: 3,
            limit: 40
        })
        expect(params).toEqual({ page: 3, limit: 40 })
    })
})