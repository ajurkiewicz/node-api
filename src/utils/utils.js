const {  pagination: { limit: defaultLimit } } = require('../emails/config')

const getPaginationLinks = ({ page, emailsCount, limit }) => {
    const { GATEWAY }= process.env
    const links = {
        first: `${GATEWAY}/api/emails?page=1&limit=${limit}`,
        last: `${GATEWAY}/api/emails?page=${Math.ceil(emailsCount/limit)}&limit=${limit}`
    }
    if (page && page > 1 && (page -1) * limit < emailsCount) {
        links.prev = `${GATEWAY}/api/emails?page=${page - 1}&limit=${limit}`
    }
    if (page && page > 0 && page * limit < emailsCount) {
        links.next = `${GATEWAY}/api/emails?page=${page + 1}&limit=${limit}`
    } else if (!page && limit < emailsCount) {
        links.next = `${GATEWAY}/api/emails?page=2&limit=${limit}`
    }
    
    return links
}

const getDBQueryOptionsForPagination = ({ page, limit }) => {
    if (page > 0) {
        return {
            skip: (page-1) * limit
        }
    }
    return {}
}

const getParsedPaginationParams = (query) => {
    const { page: queryPageId, limit: queryLimit } = query
    const page = parseInt(queryPageId) >= 0 ? parseInt(queryPageId) : 0
    const limit = parseInt(queryLimit) > 0 ? parseInt(queryLimit) : defaultLimit
    return {
        page,
        limit
    }
}

module.exports = {
    getPaginationLinks,
    getDBQueryOptionsForPagination,
    getParsedPaginationParams
}