const { statuses: { pending, sent } , priorities: { low, normal, high } } = require('../emails/config')

const getRouteForPath = (routerStack, path) => {
    return routerStack.find((layer) => layer.route.path === path).route
}

const findMethodInRouteStackByMethodType = (routerStack, methodType) => {
    return routerStack.find(layer => layer.method === methodType)
}

const generateTestData = (emailsCount) => {
    const emails = []

    for (let i = 0; i < emailsCount; i ++) {
        emails.push({
            title: `title${i+1}`,
            body: `body${i+1}`,
            sender: `test${i+1}@test.pl`,
            recipients: [`test${i+1}@test.pl` ],
            status: getStatus(emailsCount,i),
            priority: getPriority(emailsCount,i)
        })
    }

    return emails
}

const getStatus = (emailsCount, index) => {
    return index < emailsCount/2 ? pending : sent
}

const getPriority = (emailsCount,index) => {
    if (index < emailsCount/4) {
        return low
    } else if (index >= emailsCount/4 && index < emailsCount/2) {
        return normal
    } else {
        return high
    }
}

module.exports = {
    generateTestData,
    getRouteForPath,
    findMethodInRouteStackByMethodType
}