const { getRouteForPath, findMethodInRouteStackByMethodType } = require('./testUtils')

describe('Test utils tests', () => {
    let routeStack

    beforeAll(() => {
        routeStack = [
            {
                handle: [],
                name: 'bound dispatch',
                params: undefined,
                path: undefined,
                keys: [],
                regexp: { fast_star: false, fast_slash: false },
                route: { path: '/', stack: [{ name: "testGetName", method: 'get' }], methods: { get: true } }
            },
            {
                handle: [],
                name: 'bound dispatch',
                params: undefined,
                path: undefined,
                keys: [],
                regexp: { fast_star: false, fast_slash: false },
                route: { path: '/status', stack: [{ name: "testPostName", method: 'post' }], methods: { post: true }  }
            }
        ]
    })

    it('should get route for path /status', () => {
        const route = getRouteForPath(routeStack, "/status")
        expect(route).toEqual(routeStack[1].route)
    })

    it('should find get method in route stack', () => {
        const getMethod = findMethodInRouteStackByMethodType(routeStack[0].route.stack, "get")
        const postMethod = findMethodInRouteStackByMethodType(routeStack[0].route.stack, "post")
        expect(getMethod.name).toEqual('testGetName')
        expect(postMethod).toEqual(undefined)
    })

    it('should find post method in route stack', () => {
        const postMethod = findMethodInRouteStackByMethodType(routeStack[1].route.stack, "post")
        const getMethod = findMethodInRouteStackByMethodType(routeStack[1].route.stack, "get")
        expect(postMethod.name).toEqual('testPostName')
        expect(getMethod).toEqual(undefined)
    })
})