const nodemailer = require('nodemailer')
const transporter = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: 'nodejsapitest2020@gmail.com',
        pass: 'NodejsApitest2020!'
    }
})

const send = async (email) => {
    const result = await transporter.sendMail({
        from: email.sender,
        to: getRecipientsList(email),
        subject: email.title,
        html: getHtmlContent(email)
    })

    return {
        data: result,
        success: isSendingASuccess(result)
    }
}

const sendMany = async (emails) => {
    const results = []
    let info

    for (let i=0; i < emails.length; i++) {
        info = await send(emails[i])
        results.push(info)
    }

    return {
        success: isSendingManyASuccess(results),
        data: results
    }
}

const getRecipientsList = ({ recipients }) => {
    return recipients.join(",")
}

const isSendingASuccess = (result) => {
    return  result.rejected.length === 0 ? true : false
}

const isSendingManyASuccess = (results) => {
    return results.filter((r) => !r.success).length === 0
}

const getHtmlContent = ({ sender, body }) => {
    return `<b>Hi ${sender}!</b><br/>${body}</br>`
}

module.exports = {
    getHtmlContent,
    isSendingASuccess,
    isSendingManyASuccess,
    getRecipientsList,
    send,
    sendMany
}