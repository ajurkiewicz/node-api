const CONFIG = require('./config')

describe('Emails config tests', () => {
    it('should contain a correct baseUri value', () => {
        expect(CONFIG.baseUri).toBe("/api/emails")
    })

    it('should contain correct statuses', () => {
        expect(CONFIG.statuses).toEqual({
            pending: 'pending',
            sent: 'sent'
        })
    })

    it('should contain correct priorities', () => {
        expect(CONFIG.priorities).toEqual({
            low: 'low',
            normal: 'normal',
            high: 'high'
        })
    })

    it('should contain correct pagination', () => {
        expect(CONFIG.pagination).toEqual({
            limit: 25
        })
    })

    it('should contain correct response status codes', () => {
        expect(CONFIG.result.statusCodes).toEqual({
            success: 200,
            created: 201,
            notFound: 404,
            badRequest: 400,
            internalServerError: 500
        })
    })

    it('should contain correct error messages', () => {
        expect(CONFIG.errors).toEqual({
            emails: {
                invalidSender: "Invalid sender email address.",
                invalidRecipient: "Invalid recipient email address."
            }
        })
    })
})