require('./mailer.mocks').mockMailer()
const db = require('../db/mongodb')
const routes = require('./controller')
const { predefinedEmails: {
    correctEmail,
    emailWithIncorrectSender,
    emailWithIncorrectRecipients,
    emailWithOneIncorrectRecipient
} } = require('../dev/testInputData')
const { result: { statusCodes } } = require('./config')
const { success: SUCCESS, created: CREATED, badRequest: BAD_REQUEST, notFound: NOT_FOUND, internalServerError: INTERNAL_SERVER_ERROR } = statusCodes


describe('Emails router tests', () => {
    const mockResponse = () => {
        const res = {}
        res.status = jest.fn().mockReturnValue(res)
        res.send = jest.fn().mockReturnValue(res)
        return res
    }
    
    let mockedResponse
    
    const assertMockedFunctionCalls = (status) => {
        expect(mockedResponse.send).toHaveBeenCalled()
        expect(mockedResponse.status).toHaveBeenCalledWith(status)
    }

    beforeAll(async () => {
        await db.connect()
    })

    beforeEach(async () => {
        mockedResponse = mockResponse()
    })

    afterAll(async () => {
        await db.disconnect()
    })

    it('should correctly execute getAllEmails without query params', async () => {
        await routes.getAllEmails({
            query: {
            }
        }, mockedResponse)
        
        assertMockedFunctionCalls(SUCCESS)
    })

    it('should correctly execute getAllEmails with correct query params', async () => {
        await routes.getAllEmails({
            query: {
                limit: 10,
                page: 2
            }
        }, mockedResponse)
        assertMockedFunctionCalls(SUCCESS)
    })

    it('should correctly execute getAllEmails with incorrect query params', async () => {
        await routes.getAllEmails({
            query: {
                limit: -10,
                page: -1
            }
        }, mockedResponse)
        assertMockedFunctionCalls(SUCCESS)
    })

    it('should not be able to save email with missing request params', async () => {
        await routes.createEmail({}, mockedResponse)
        assertMockedFunctionCalls(BAD_REQUEST)
    })

    it('should correctly save email', async () => {
        const req = {
            body: correctEmail
        }

        await routes.createEmail(req, mockedResponse)
        assertMockedFunctionCalls(CREATED)
    })

    it('should not be able to save email with incorrect sender(validation issue)', async () => {
        const req = {
            body: emailWithIncorrectSender
        }

        await routes.createEmail(req, mockedResponse)
        assertMockedFunctionCalls(BAD_REQUEST)
    })

    it('should not be able to save email with incorrect recipients(validation issue)', async () => {
        const req = {
            body: emailWithIncorrectRecipients
        }

        await routes.createEmail(req, mockedResponse)
        assertMockedFunctionCalls(BAD_REQUEST)
    })

    it('should not be able to save email with one incorrect recipient(validation issue)', async () => {
        const req = {
            body: emailWithOneIncorrectRecipient
        }

        await routes.createEmail(req, mockedResponse)
        assertMockedFunctionCalls(BAD_REQUEST)
    })

    it('should get email for correct id', async () => {
        const req = {
            params: {
                id: "5edd54705ebb9b86b8283e5a"
            }
        }
        await routes.getEmailById(req, mockedResponse)
        assertMockedFunctionCalls(SUCCESS)
    })

    it('should not get email for incorrect id', async () => {
        const req = {
            params: {
                id: "9eda1329f7fd1e4c8c75e6c2"
            }
        }
        await routes.getEmailById(req, mockedResponse)
        assertMockedFunctionCalls(NOT_FOUND)
    })

    it('should not get email for incorret id format', async () => {
        const req = {
            params: {
                id: "001"
            }
        }
        await routes.getEmailById(req, mockedResponse)
        assertMockedFunctionCalls(INTERNAL_SERVER_ERROR)
    })

    it('should get correct email status', async () => {
        const req = {
            params: {
                id: "5edd54705ebb9b86b8283e5a"
            }
        }
        await routes.getEmailStatusById(req, mockedResponse)
        assertMockedFunctionCalls(SUCCESS)
    })

    it('should patch email status after sending', async () => {
        const req = {
            params: {
                id: "5edd54705ebb9b86b8283e5a"
            }
        }
        await routes.sendEmail(req, mockedResponse)
        assertMockedFunctionCalls(SUCCESS)
    })
})