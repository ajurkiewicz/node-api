const { prefix, services: { emails } } = require('../config')

module.exports = {
    baseUri: `/${prefix}/${emails}`,
    statuses: {
        pending: 'pending',
        sent: 'sent'
    },
    priorities: {
        low: 'low',
        normal: 'normal',
        high: 'high'
    },
    pagination: {
        limit: 25
    },
    result: {
        statusCodes: {
            success: 200,
            created: 201,
            notFound: 404,
            badRequest: 400,
            internalServerError: 500
        }
    },
    errors: {
        emails: {
            invalidSender: "Invalid sender email address.",
            invalidRecipient: "Invalid recipient email address."
        }
    }
}
