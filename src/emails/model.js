const mongoose = require('mongoose')
const { isEmail } = require('validator')
const { statuses: { pending }, priorities: { normal }, errors } = require('./config')
const { invalidSender: INVALID_SENDER_ERROR_MESSAGE, invalidRecipient: INVALID_RECIPIENT_ERROR_MESSAGE } = errors.emails

const Email = mongoose.model('Email', {
    sender: {
        type: String,
        required: true,
        validate: [senderEmail => isEmail(senderEmail), INVALID_SENDER_ERROR_MESSAGE ],
        trim: true,
        lowercase: true
    },
    recipients: {
        type: [{
            type: String,
            trim: true,
            lowercase: true,
            validate: [recipientEmail => isEmail(recipientEmail), INVALID_RECIPIENT_ERROR_MESSAGE ]
        }],
        required: true,
        default: undefined
    },
    title: {
        type: String,
        required: true,
        trim: true
    },
    body: {
        type: String,
        required: true,
        trim: true
    },
    status: {
        type: String,
        default: pending
    },
    priority: {
        type: String,
        default: normal
    }
})

module.exports = Email