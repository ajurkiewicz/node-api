const express = require('express')
const emailsRouter = require('./router')
const app = express()
const router  = emailsRouter(app)
const { getRouteForPath, findMethodInRouteStackByMethodType } = require('../utils/testUtils')

describe('Emails router tests', () => {

    it('should return correct number of routes', async () => {
        expect(router.stack.length).toBe(6)
    })

    it('should return correct methods for route /', async () => {
        const route = getRouteForPath(router.stack,'/')
        const getMethod = findMethodInRouteStackByMethodType(route.stack, 'get')
        const postMethod = findMethodInRouteStackByMethodType(route.stack, 'post')
        const { path, methods } = route

        expect(path).toBe('/')
        expect(methods).toEqual({
            get: true,
            post: true
        })
        expect(getMethod.name).toBe('getAllEmails')
        expect(postMethod.name).toBe('createEmail')
    })

    it('should return correct methods for route /status', async () => {
        const route = getRouteForPath(router.stack,'/status')
        const getMethod = findMethodInRouteStackByMethodType(route.stack, 'get')
        const { path, methods } = route

        expect(path).toBe('/status')
        expect(methods).toEqual({
            get: true
        })
        expect(getMethod.name).toBe('getEmailsStatusesCounts')
    })

    it('should return correct methods for route /send', async () => {
        const route = getRouteForPath(router.stack,'/send')
        const patchMethod = findMethodInRouteStackByMethodType(route.stack, 'patch')
        const { path, methods } = route

        expect(path).toBe('/send')
        expect(methods).toEqual({
            patch: true
        })
        expect(patchMethod.name).toBe('sendEmails')
    })

    it('should return correct methods for route /:id', async () => {
        const route = getRouteForPath(router.stack,'/:id')
        const getMethod = findMethodInRouteStackByMethodType(route.stack,'get')
        const { path, methods } = route

        expect(path).toBe('/:id')
        expect(methods).toEqual({ get: true })
        expect(getMethod.name).toBe('getEmailById')
    })

    it('should return correct methods for route /:id/status', async () => {
        const route = getRouteForPath(router.stack,'/:id/status')
        const getMethod = findMethodInRouteStackByMethodType(route.stack,'get')
        const { path, methods } = route

        expect(path).toBe('/:id/status')
        expect(methods).toEqual({ get: true })
        expect(getMethod.name).toBe('getEmailStatusById')
    })

    it('should return correct methods for route /:id/send', async () => {
        const route = getRouteForPath(router.stack,'/:id/send')
        const patchMethod = findMethodInRouteStackByMethodType(route.stack,'patch')
        const { path, methods } = route

        expect(path).toBe('/:id/send')
        expect(methods).toEqual({ patch: true })
        expect(patchMethod.name).toBe('sendEmail')
    })
})