const mockedMailer = {
    send: jest.fn().mockReturnValue({
        success: true
    }),
    sendMany: jest.fn().mockReturnValue({
        success: true
    })
}

const mockedTransporter = {
    sendMail: jest.fn().mockReturnValue({
        rejected: [],
        response: '250 2.0.0 OK'
    })
}

const mockedNodeMailer = {
    createTransport: jest.fn().mockReturnValue(mockedTransporter)
}

const mockMailer = () => {
    jest.mock('./mailer', () => mockedMailer)
}

const mockNodeMailer = () => {
    jest.mock('nodemailer', () => mockedNodeMailer)
    return mockedTransporter
}

module.exports = {
    mockNodeMailer,
    mockMailer
}

