const express = require('express')
const router = express.Router()
const {
    getAllEmails,
    getEmailById,
    getEmailStatusById,
    getEmailsStatusesCounts,
    sendEmail,
    createEmail,
    sendEmails
} = require('./controller')
const { baseUri: BASE_URI } = require('./config')


module.exports = function(app) {
    router.route("/")
        .get(getAllEmails)
        .post(createEmail)

    router.route("/status")
        .get(getEmailsStatusesCounts)

    router.route("/send")
        .patch(sendEmails)

    router.route("/:id")
        .get(getEmailById)

    router.route("/:id/status")
        .get(getEmailStatusById)

    router.route("/:id/send")
        .patch(sendEmail)

    app.use(BASE_URI, router)
    return router
}