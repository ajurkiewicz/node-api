const Email = require('./model')
const { statuses: { pending }, priorities: { normal } } = require('./config')
const { predefinedEmails: { correctEmail } } = require('../dev/testInputData')

describe('Email model  tests', () => {
    it('should create correct email model with default values', () => {
        const email = new Email({
            ...correctEmail
        })


        expect(email.sender).toEqual('test@test.pl')
        expect(email.status).toEqual(pending)
        expect(email.priority).toEqual(normal)
    })

    it('should create correct email model with trimmed values', () => {
        const email = new Email({
            sender: "   test@test.pl   ",
            recipients: [
                "test1@test.pl   ",
                "test2@test.pl   "
            ],
            title: 'title test   ',
            body: 'body test    '
        })


        expect(email.sender).toEqual("test@test.pl")
        expect(email.title).toEqual("title test")
        expect(email.body).toEqual("body test")
    })
})