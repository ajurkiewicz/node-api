const nodeMailerTransporterMock = require('./mailer.mocks').mockNodeMailer()
const mailer = require('./mailer')
const { predefinedEmails: { correctEmail } } = require('../dev/testInputData')

describe('Mailer tests', () => {
    it('should get a correct html value from Email model', () => {
        const expectedHtml = `<b>Hi test@test.pl!</b><br/>test body</br>`
        expect(mailer.getHtmlContent(correctEmail)).toBe(expectedHtml)
    })

    it('should get a correct formatted recipients list', () => {
        const expectedRecipientsList = 'test@test.pl,test2@test.pl'
        expect(mailer.getRecipientsList(correctEmail)).toBe(expectedRecipientsList)
    })

    it('should return sending success for provided result', () => {
        const result = {
            rejected: []
        }
        expect(mailer.isSendingASuccess(result)).toBe(true)
    })

    it('should return sending failure for provided result', () => {
        const result = {
            rejected: [ 'test@test.pl' ]
        }
        expect(mailer.isSendingASuccess(result)).toBe(false)
    })

    it('should return sending many success for provided results', () => {
        const results = [ { success: true }, { success: true } ]
        expect(mailer.isSendingManyASuccess(results)).toBe(true)
    })

    it('should return sending many failure for provided results', () => {
        const results = [ { success: false }, { success: true } ]
        expect(mailer.isSendingManyASuccess(results)).toBe(false)
    })

    it('should send email', async () => {
        const info = await mailer.send(correctEmail)
        expect(nodeMailerTransporterMock.sendMail).toBeCalledTimes(1)
        expect(nodeMailerTransporterMock.sendMail).toBeCalledWith({
            from: correctEmail.sender,
            to: correctEmail.recipients.join(","),
            subject: correctEmail.title,
            html: mailer.getHtmlContent(correctEmail)
        })
        expect(info.success).toEqual(true)
        expect(info.data.rejected).toEqual([])
        expect(info.data.response).toContain('250 2.0.0 OK')
    })

    it('should send emails', async () => {
        const info = await mailer.sendMany([
            correctEmail,
            correctEmail
        ])
        expect(info.success).toEqual(true)
        expect(info.data[0].success).toEqual(true)
        expect(info.data[0].data.rejected).toEqual([])
        expect(info.data[1].success).toEqual(true)
        expect(info.data[1].data.rejected).toEqual([])
    })
})