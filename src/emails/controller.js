const Email = require('./model')
const mailer = require('./mailer')
const { statuses: { pending, sent } } = require('./config')
const { getPaginationLinks, getDBQueryOptionsForPagination, getParsedPaginationParams } = require('../utils/utils')
const { result: { statusCodes } } = require('./config')
const { success: SUCCESS, created: CREATED, badRequest: BAD_REQUEST, notFound: NOT_FOUND, internalServerError: INTERNAL_SERVER_ERROR } = statusCodes

const getEmailById = async (req, res) => {
    const { id: _id } = req.params
    
    try {
        const email = await Email.findById(_id)
        if (!email) {
            return res.status(NOT_FOUND).send()
        }
        res.status(SUCCESS).send({
            data: email
        })
    } catch (error) {
        res.status(INTERNAL_SERVER_ERROR).send()
    }
}

const getAllEmails = async (req,res) => {
    try {
        const { page, limit } = getParsedPaginationParams(req.query)
        const options = getDBQueryOptionsForPagination({ page, limit })
        const emails = await Email.find({},null,options).limit(limit)
        const emailsCount = await Email.countDocuments()
        const data = {
            totalEmailsCount: emailsCount,
            emailsPerPage: limit,
            emails
        }
        const links = getPaginationLinks({
            page,
            emailsCount,
            limit
        })
        res.status(SUCCESS).send({
            data,
            links
        })
    } catch (error) {
        res.status(INTERNAL_SERVER_ERROR).send()
    }
}

const createEmail = async (req, res) => {
    try {
        const email = new Email(req.body)
        await email.save()
        res.status(CREATED).send({
            data: email
        })
    } catch (error) {
        res.status(BAD_REQUEST).send(error)
    }
  
}

const sendEmail = async (req, res) => {
    const { id: _id } = req.params

    try {
        const email = await Email.findById(_id)
        if (!email) {
            return res.status(NOT_FOUND).send()
        }
        const sendingResult = await mailer.send(email)
        if (sendingResult.success) {
            const updatedEmail = await Email.findByIdAndUpdate(_id, { status: sent },{
                new: true,
                useFindAndModify: false
            })
            
            if (updatedEmail) {
                res.status(SUCCESS).send({
                    data: updatedEmail
                })
            }
        }
        
    } catch (error) {
        res.status(BAD_REQUEST).send(error)
    }
}

const sendEmails = async (req, res) => {
    try {
        const emails = await Email.find({ status: pending })
        const sendingResult = await mailer.sendMany(emails)

        if (sendingResult.success) {
            const updateResult = await Email.updateMany({ status: pending }, { "$set": { status: sent } })
            res.status(SUCCESS).send({
                data: updateResult
            })
        }

    } catch (error) {
        res.status(INTERNAL_SERVER_ERROR).send()
    }
}

const getEmailStatusById = async (req, res) => {
    const { id: _id } = req.params

    try {
        const email = await Email.findById(_id)
        if (!email) {
            return res.status(NOT_FOUND).send()
        }
        res.status(SUCCESS).send({
            data: {
                status: email.status
            }
        })
    } catch (error) {
        res.status(INTERNAL_SERVER_ERROR).send()
    }
}

const getEmailsStatusesCounts = async (req, res) => {
    try {
        const pendingEmailsCount = await Email.countDocuments({ status: pending })
        const sentEmailsCount = await Email.countDocuments({ status: sent })
        
        res.status(SUCCESS).send({
            data: {
                pending: pendingEmailsCount,
                sent: sentEmailsCount
            }

        })
    } catch (error) {
        res.status(INTERNAL_SERVER_ERROR).send()
    }
}


module.exports = {
    getAllEmails,
    getEmailById,
    getEmailStatusById,
    getEmailsStatusesCounts,
    createEmail,
    sendEmail,
    sendEmails
}