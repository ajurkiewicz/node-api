# Api - coding challenge

### SETUP

* Install nodeJS >= 8.3.0
* Install MongoDB on your computer. Please visit https://docs.mongodb.com/manual/installation for more information.
* Install all dependencies with nmp -> `npm i`
* Set correct path in start-mongodb script `--dbpath=PATH_TO_STORE_DB_FILES`. Please visit https://docs.mongodb.com/manual/reference/program/mongod/#cmdoption-mongod-dbpath for more information.

### Running the application
* Run `npm run start`

### Running unit tests
* Run `npm run test`

### Database operations
* Run `npm run reset-db` to load init data to emails collection in database

### Postman
* You can load postman `emails-api.postman_collection.json` collection in Postman to further test api using predefined test scenarios. You can download the app from https://www.postman.com/downloads.